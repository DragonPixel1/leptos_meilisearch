/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use meilisearch_sdk::{MeilisearchCommunicationError, MeilisearchError};
use thiserror::Error;

/// This is a wrapper class for the `meilisearch_sdk::errors::Error`.
#[derive(Debug, Clone, Error)]
#[non_exhaustive]
pub enum Error {
    /// The exhaustive list of Meilisearch errors: <https://github.com/meilisearch/specifications/blob/main/text/0061-error-format-and-definitions.md>
    ///
    /// Also check out: <https://github.com/meilisearch/Meilisearch/blob/main/meilisearch-error/src/lib.rs>
    #[error(transparent)]
    Meilisearch(#[from] MeilisearchError),

    #[error(transparent)]
    MeilisearchCommunication(#[from] MeilisearchCommunicationError),

    /// There is no Meilisearch server listening on the [specified host]
    /// (../client/struct.Client.html#method.new).
    #[error("The Meilisearch server can't be reached.")]
    UnreachableServer,

    /// The Meilisearch server returned an invalid JSON for a request.
    #[error("Error parsing response JSON: {}", .0)]
    ParseError(String),

    /// A timeout happened while waiting for an update to complete.
    #[error("A task did not succeed in time.")]
    Timeout,

    /// This Meilisearch SDK generated an invalid request (which was not sent).
    ///
    /// It probably comes from an invalid API key resulting in an invalid HTTP header.
    #[error("Unable to generate a valid HTTP request. It probably comes from an invalid API key.")]
    InvalidRequest,

    /// Can't call this method without setting an api key in the client.
    #[error("You need to provide an api key to use the `{0}` method.")]
    CantUseWithoutApiKey(String),

    /// It is not possible to generate a tenant token with a invalid api key.
    ///
    /// Empty strings or with less than 8 characters are considered invalid.
    #[error("The provided api_key is invalid.")]
    TenantTokensInvalidApiKey,

    /// It is not possible to generate an already expired tenant token.
    #[error("The provided expires_at is already expired.")]
    TenantTokensExpiredSignature,

    /// When jsonwebtoken cannot generate the token successfully.
    #[error("Impossible to generate the token, jsonwebtoken encountered an error: {}", .0)]
    InvalidTenantToken(#[from] jsonwebtoken::errors::Error),

    /// The http client encountered an error.
    #[error("HTTP request failed: {}", .0)]
    HttpError(String),

    // The library formating the query parameters encountered an error.
    #[error("Internal Error: could not parse the query parameters: {}", .0)]
    Yaup(String),

    // The library validating the format of an uuid.
    #[error("The uid of the token has bit an uuid4 format: {}", .0)]
    Uuid(#[from] uuid::Error),

    // Error thrown in case the version of the Uuid is not v4.
    #[error("The uid provided to the token is not of version uuidv4")]
    InvalidUuid4Version,

    #[error("an unknown error is occured: {0}")]
    Unknown(String),
}

impl From<meilisearch_sdk::Error> for Error {
    fn from(value: meilisearch_sdk::Error) -> Self {
        match value {
            meilisearch_sdk::Error::Meilisearch(error) => Error::Meilisearch(error),
            meilisearch_sdk::Error::MeilisearchCommunication(error) => {
                Error::MeilisearchCommunication(error)
            }
            meilisearch_sdk::Error::UnreachableServer => Error::UnreachableServer,
            meilisearch_sdk::Error::ParseError(error) => Error::ParseError(error.to_string()),
            meilisearch_sdk::Error::Timeout => Error::Timeout,
            meilisearch_sdk::Error::InvalidRequest => Error::InvalidRequest,
            meilisearch_sdk::Error::CantUseWithoutApiKey(error) => {
                Error::CantUseWithoutApiKey(error)
            }
            meilisearch_sdk::Error::TenantTokensInvalidApiKey => Error::TenantTokensInvalidApiKey,
            meilisearch_sdk::Error::TenantTokensExpiredSignature => {
                Error::TenantTokensExpiredSignature
            }
            meilisearch_sdk::Error::InvalidTenantToken(error) => Error::InvalidTenantToken(error),
            meilisearch_sdk::Error::HttpError(error) => Error::HttpError(error.to_string()),
            meilisearch_sdk::Error::Yaup(error) => Error::Yaup(error.to_string()),
            meilisearch_sdk::Error::InvalidUuid4Version => Error::InvalidUuid4Version,
            error => Error::Unknown(error.to_string()),
        }
    }
}
