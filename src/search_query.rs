/*
* The MIT License (MIT)
*
* Copyright (c) 2023 Daniél Kerkmann <daniel@kerkmann.dev>
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

use derive_builder::Builder;
use meilisearch_sdk::Index;

/// This is a wrapper class for the `meilisearch_sdk::search::SearchQuery`.
#[derive(Debug, Default, Clone, PartialEq, Builder)]
#[builder(setter(into, strip_option), default)]
#[builder(pattern = "immutable")]
pub struct SearchQuery {
    /// The text that will be searched for among the documents.
    pub query: Option<String>,

    /// The number of documents to skip.
    ///
    /// If the value of the parameter `offset` is `n`, the `n` first documents (ordered by relevance) will not be returned.
    /// This is helpful for pagination.
    ///
    /// Example: If you want to skip the first document, set offset to `1`.
    pub offset: Option<usize>,

    /// The maximum number of documents returned.
    ///
    /// If the value of the parameter `limit` is `n`, there will never be more than `n` documents in the response.
    /// This is helpful for pagination.
    ///
    /// Example: If you don't want to get more than two documents, set limit to `2`.
    ///
    /// **Default: `20`**
    pub limit: Option<usize>,

    /// The page number on which you paginate.
    ///
    /// Pagination starts at 1. If page is 0, no results are returned.
    ///
    /// **Default: None unless `hits_per_page` is defined, in which case page is `1`**
    pub page: Option<usize>,

    /// The maximum number of results in a page. A page can contain less results than the number of hits_per_page.
    ///
    /// **Default: None unless `page` is defined, in which case `20`**
    pub hits_per_page: Option<usize>,

    /// Filter applied to documents.
    ///
    /// Read the [dedicated guide](https://www.meilisearch.com/docs/learn/advanced/filtering) to learn the syntax.
    pub filter: Option<String>,

    /// Facets for which to retrieve the matching count.
    ///
    /// Can be set to a [wildcard value](enum.Selectors.html#variant.All) that will select all existing attributes.
    ///
    /// **Default: all attributes found in the documents.**
    pub facets: Option<Selectors<Vec<&'static str>>>,

    /// Attributes to sort.
    pub sort: Option<Vec<&'static str>>,

    /// Attributes to perform the search on.
    ///
    /// Specify the subset of searchableAttributes for a search without modifying Meilisearch’s index settings.
    ///
    /// **Default: all searchable attributes found in the documents.**
    pub attributes_to_search_on: Option<Vec<&'static str>>,

    /// Attributes to display in the returned documents.
    ///
    /// Can be set to a [wildcard value](enum.Selectors.html#variant.All) that will select all existing attributes.
    ///
    /// **Default: all attributes found in the documents.**
    pub attributes_to_retrieve: Option<Selectors<Vec<&'static str>>>,

    /// Attributes whose values have to be cropped.
    ///
    /// Attributes are composed by the attribute name and an optional `usize` that overwrites the `crop_length` parameter.
    ///
    /// Can be set to a [wildcard value](enum.Selectors.html#variant.All) that will select all existing attributes.
    pub attributes_to_crop: Option<Selectors<Vec<AttributeToCrop>>>,

    /// Maximum number of words including the matched query term(s) contained in the returned cropped value(s).
    ///
    /// See [attributes_to_crop](#structfield.attributes_to_crop).
    ///
    /// **Default: `10`**
    pub crop_length: Option<usize>,

    /// Marker at the start and the end of a cropped value.
    ///
    /// ex: `...middle of a crop...`
    ///
    /// **Default: `...`**
    pub crop_marker: Option<String>,

    /// Attributes whose values will contain **highlighted matching terms**.
    ///
    /// Can be set to a [wildcard value](enum.Selectors.html#variant.All) that will select all existing attributes.
    pub attributes_to_highlight: Option<Selectors<Vec<&'static str>>>,

    /// Tag in front of a highlighted term.
    ///
    /// ex: `<mytag>hello world`
    ///
    /// **Default: `<em>`**
    pub highlight_pre_tag: Option<String>,

    /// Tag after the a highlighted term.
    ///
    /// ex: `hello world</ mytag>`
    ///
    /// **Default: `</em>`**
    pub highlight_post_tag: Option<String>,

    /// Defines whether an object that contains information about the matches should be returned or not.
    ///
    /// **Default: `false`**
    pub show_matches_position: Option<bool>,

    /// Defines whether to show the relevancy score of the match.
    ///
    /// **Default: `false`**
    pub show_ranking_score: Option<bool>,

    /// Defines the strategy on how to handle queries containing multiple words.
    pub matching_strategy: Option<MatchingStrategies>,
}

type AttributeToCrop = (&'static str, Option<usize>);

#[derive(Debug, Clone, PartialEq)]
pub enum Selectors<T> {
    Some(T),
    All,
}

impl<T> From<Selectors<T>> for meilisearch_sdk::Selectors<T> {
    fn from(val: Selectors<T>) -> Self {
        match val {
            Selectors::Some(value) => meilisearch_sdk::Selectors::Some(value),
            Selectors::All => meilisearch_sdk::Selectors::All,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum MatchingStrategies {
    ALL,
    LAST,
}

impl From<&MatchingStrategies> for meilisearch_sdk::MatchingStrategies {
    fn from(val: &MatchingStrategies) -> Self {
        match val {
            MatchingStrategies::ALL => meilisearch_sdk::MatchingStrategies::ALL,
            MatchingStrategies::LAST => meilisearch_sdk::MatchingStrategies::LAST,
        }
    }
}

impl SearchQuery {
    #[must_use]
    pub fn search<'a>(&'a self, index: &'a Index) -> meilisearch_sdk::SearchQuery<'a> {
        let mut search_query = meilisearch_sdk::SearchQuery::new(index);

        search_query.query = self.query.as_deref();
        search_query.offset = self.offset;
        search_query.limit = self.limit;
        search_query.page = self.page;
        search_query.hits_per_page = self.hits_per_page;
        if let Some(filter) = &self.filter {
            search_query.with_filter(filter);
        }
        if let Some(Selectors::Some(facets)) = &self.facets {
            search_query.with_facets(meilisearch_sdk::Selectors::Some(facets.as_slice()));
        }
        if let Some(sort) = &self.sort {
            search_query.with_sort(sort.as_slice());
        }
        if let Some(attributes_to_search_on) = &self.attributes_to_search_on {
            search_query.with_attributes_to_search_on(attributes_to_search_on.as_slice());
        }
        if let Some(Selectors::Some(attributes_to_retrieve)) = &self.attributes_to_retrieve {
            search_query.with_attributes_to_retrieve(meilisearch_sdk::Selectors::Some(
                attributes_to_retrieve.as_slice(),
            ));
        }
        if let Some(Selectors::Some(attributes_to_crop)) = &self.attributes_to_crop {
            search_query.with_attributes_to_crop(meilisearch_sdk::Selectors::Some(
                attributes_to_crop.as_slice(),
            ));
        }
        search_query.crop_length = self.crop_length;
        search_query.crop_marker = self.crop_marker.as_deref();
        if let Some(Selectors::Some(attributes_to_highlight)) = &self.attributes_to_highlight {
            search_query.with_attributes_to_highlight(meilisearch_sdk::Selectors::Some(
                attributes_to_highlight.as_slice(),
            ));
        }
        search_query.highlight_pre_tag = self.highlight_pre_tag.as_deref();
        search_query.highlight_post_tag = self.highlight_post_tag.as_deref();
        search_query.show_matches_position = self.show_matches_position;
        search_query.show_ranking_score = self.show_ranking_score;
        if let Some(matching_strategy) = &self.matching_strategy {
            search_query.with_matching_strategy(matching_strategy.into());
        }

        search_query
    }
}
